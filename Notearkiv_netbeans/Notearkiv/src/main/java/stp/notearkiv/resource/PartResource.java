package stp.notearkiv.resource;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import stp.notearkiv.entity.Part;

/**
 *
 * @author Team STP
 */
@Stateless
@Path("/parts")

public class PartResource {

    @PersistenceContext(unitName = "Notearkivet")
    private EntityManager em;

    @Inject
    SessionResource sr;

    private final String selectParts = "SELECT p from Part p";

    /**
     * Return a list of all registered parts
     *
     * @return a list of all parts
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Part> getAllParts() {
        return em.createQuery(selectParts, Part.class).getResultList();
    }

    /**
     * Creates a new part
     *
     * @param cookie
     * @param name name to be given to the new part
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.security.spec.InvalidKeySpecException
     * @return a list of the added parts (list of null, if none was added)
     */
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces({MediaType.APPLICATION_JSON})
    public List<Part> addPart(
            @CookieParam("access_token") Cookie cookie,
            @FormParam("part") String name)
            throws NoSuchAlgorithmException, InvalidKeySpecException {

        List<Part> error = null;

        if (!(sr.isAdmin(cookie)) && !(sr.isArkivansvarlig(cookie))) {
            return error;
        } else {
            List result1 = em.createQuery(selectParts + " WHERE lower (p.name) = lower (:paramID)", Part.class).setParameter("paramID", name).getResultList();
            if (result1.isEmpty()) {
                // create the new part with all arguments
                Part p = new Part(name);
                em.persist(p);
                return em.createQuery(selectParts, Part.class).getResultList();
            }
            return error;
        }
    }
}
