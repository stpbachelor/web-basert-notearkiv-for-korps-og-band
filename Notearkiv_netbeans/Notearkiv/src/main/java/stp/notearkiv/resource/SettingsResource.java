package stp.notearkiv.resource;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import stp.notearkiv.entity.SystemSetting;

/**
 * Resource class for handling settings.
 *
 * @author Team STP
 */
@Stateless
@Path("")
public class SettingsResource {
    
    @PersistenceContext(unitName = "Notearkivet")
    private EntityManager em;
    
    @Inject
    SessionResource sr;
    
    SystemSetting s = null;

    public SettingsResource() {
    }
    
    /**
     * Returns a list of all settings
     *
     * @return a list of all settings
     */
    @GET
    @Path("/settings")
    @Produces({MediaType.APPLICATION_JSON})
    public List<SystemSetting> getAllSettings() {
        return em.createQuery("SELECT s from SystemSetting s", SystemSetting.class).getResultList();
    }
    
    /**
     * Post new settings
     * 
     * @param cookie
     * @param serverurl
     * @param servicesurl
     * @param version_number
     * @param email_address
     * @param email_password
     * @param email_smtp
     * @param email_port
     * @param admin_name
     * @param admin_email
     * @param contact_email
     * @param about_info
     * @return a list of all settings
     */
    @POST
    @Path("/settings")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public List<SystemSetting> editSettings(
            @CookieParam("access_token") Cookie cookie,
            @FormParam("serverurl") String serverurl,
            @FormParam("servicesurl") String servicesurl,
            @FormParam("version_number") String version_number,
            @FormParam("email_address") String email_address,
            @FormParam("email_password") String email_password,
            @FormParam("email_smtp") String email_smtp,
            @FormParam("email_port") String email_port,
            @FormParam("admin_name") String admin_name,
            @FormParam("admin_email") String admin_email,
            @FormParam("contact_email") String contact_email,
            @FormParam("about_info") String about_info) {
        
        if(!sr.isAdmin(cookie) && !sr.isArkivansvarlig(cookie)){
            List<SystemSetting> no = new ArrayList<>();
            return no; 
        }
        
        s = em.find(SystemSetting.class, "system_settings");
        s.setServerurl(serverurl);
        s.setServicesurl(servicesurl);
        s.setEmail_address(email_address);
        s.setEmail_password(email_password);
        s.setEmail_smtp(email_smtp);
        s.setEmail_port(email_port);
        s.setAdmin_name(admin_name);
        s.setAdmin_email(admin_email);
        s.setContact_email(contact_email);
        s.setAbout_info(about_info);
        em.merge(s);
        
        return em.createQuery("SELECT s from SystemSetting s", SystemSetting.class).getResultList();
    }
    
    /**
     *
     * @return
     */
    public String getAdminName() {
        String result = "";
        s = em.find(SystemSetting.class, "system_settings");
        if (s != null) {
            result = s.getAdmin_name();
        }
        return result;
    }
    
    /**
     *
     * @return
     */
    public String getAdminEmail() {
        String result = "";
        s = em.find(SystemSetting.class, "system_settings");
        if (s != null) {
            result = s.getAdmin_email();
        }
        return result;
    }
    
    /**
     *
     * @return
     */
    public String getContactEmail() {
        String result = "";
        s = em.find(SystemSetting.class, "system_settings");
        if (s != null) {
            result = s.getContact_email();
        }
        return result;
    }

    /**
     *
     * @return
     */
    public String getApplicationVersion() {
        String result = "";
        if (s != null) {
            s = em.find(SystemSetting.class, "system_settings");
            result = s.getVersion_number();
        }
        return result;
    }
    
    /**
     *
     * @return
     */
    public String getEmailPort() {
        String result = "";
        s = em.find(SystemSetting.class, "system_settings");
        if (s != null) {
            result = s.getEmail_port();
        }
        return result;
    }
    
    /**
     *
     * @return
     */
    public String getEmailAddress() {
        String result = "";
        s = em.find(SystemSetting.class, "system_settings");
        if (s != null) {
            result = s.getEmail_address();
        }
        return result;
    }

    /**
     *
     * @return
     */
    public String getEmailPassword() {
        String result = "";
        s = em.find(SystemSetting.class, "system_settings");
        if (s != null) {
            result = s.getEmail_password();
        }
        return result;
    }

    /**
     *
     * @return
     */
    public String getServerUrl() {
        String result = "";
        s = em.find(SystemSetting.class, "system_settings");
        if (s != null) {
            result = s.getServerurl();
        }
        return result;
    }
    
    /**
     *
     * @return
     */
    public String getServicesUrl() {
        String result = "";
        s = em.find(SystemSetting.class, "system_settings");
        if (s != null) {
            result = s.getServicesurl();
        }
        return result;
    }

    /**
     *
     * @return
     */
    public String getEmailSmtp() {
        String result = "";
        s = em.find(SystemSetting.class, "system_settings");
        if (s != null) {
            result = s.getEmail_smtp();
        }
        return result;
    }
    
    /**
     *
     * @return
     */
    public String getAboutInfo() {
        String result = "";
        s = em.find(SystemSetting.class, "system_settings");
        if (s != null) {
            result = s.getAbout_info();
        }
        return result;
    }
}