package stp.notearkiv.resource;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import stp.notearkiv.entity.Instrument;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Team STP
 */
@Stateless
@Path("/instruments")

public class InstrumentResource {

    @PersistenceContext(unitName = "Notearkivet")
    private EntityManager em;

    @Inject
    SessionResource sr;

    private final String selectInstruments = "SELECT i from Instrument i";

    /**
     * Return a list of all registered instruments
     *
     * @return a list of all instruments
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Instrument> getAllInstruments() {
        return em.createQuery(selectInstruments, Instrument.class).getResultList();
    }

    /**
     * Create a new instrument
     *
     * @param cookie
     * @param name name to be given to the new instrument
     * @return a list with the new instrument
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.security.spec.InvalidKeySpecException
     */
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces({MediaType.APPLICATION_JSON})
    public List<Instrument> addInstrument(@CookieParam("access_token") Cookie cookie, @FormParam("name") String name) throws NoSuchAlgorithmException, InvalidKeySpecException {

        List<Instrument> error = null;

        if (!(sr.isAdmin(cookie)) && !(sr.isArkivansvarlig(cookie))) {
            return error;
        } else {
            List result1 = em.createQuery(selectInstruments + " WHERE lower (i.name) = lower (:paramID)", Instrument.class).setParameter("paramID", name).getResultList();
            if (result1.isEmpty()) {
                Instrument i = new Instrument(name);
                em.persist(i);
                return em.createQuery(selectInstruments, Instrument.class).getResultList();
            }
            return error;
        }
    }
}
