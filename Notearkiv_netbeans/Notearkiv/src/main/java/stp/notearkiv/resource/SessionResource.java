package stp.notearkiv.resource;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import stp.notearkiv.PasswordEncryptionService;
import stp.notearkiv.entity.Role;
import stp.notearkiv.entity.User;

/**
 *
 * @author Team STP
 */
@Stateless
@Path("")
public class SessionResource {

    @PersistenceContext(unitName = "Notearkivet")
    private EntityManager em;

    private final Key key = Resource.KEY;

    /**
     *
     * @param email
     * @param password
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    @Path("/login")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public Response login(
            @FormParam("email") String email,
            @FormParam("password") String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        boolean result;

        List contain = em.createQuery("SELECT u FROM User u WHERE (u.user_name) = (:paramID)", User.class).setParameter("paramID", email).getResultList();

        User temp;
        if (!contain.isEmpty()) {
            // ignores multiple results
            temp = (User) contain.get(0);

            if (!temp.isActive()) {
                return Response.ok("ikke aktiv").build();
            }

            PasswordEncryptionService check = new PasswordEncryptionService();
            result = check.authenticate(password, temp.getPassword(), temp.getSalt());
            if (result == true) {

                temp.updateLogin_count();
                temp.updateDate_login();
                em.persist(temp);

                String s = "";
                for (Role tempRoles : temp.getRoles()) {
                    s += tempRoles.getName() + " ";
                }
                int tempInt = temp.getUser_id();
                String tempString = Integer.toString(tempInt);
                String compactJws = Jwts.builder().setId(tempString).setSubject(s).signWith(SignatureAlgorithm.HS512, key).compact();
                NewCookie cookie = new NewCookie("access_token", compactJws);
                return Response.ok("OK").cookie(cookie).build();
            }
            return Response.ok("feil passord").build();
        }
        return Response.ok("ingen bruker").build();
    }

    /**
     *
     * @param cookie
     * @return
     */
    @Path("/session")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response sessionCheck(@CookieParam("access_token") Cookie cookie) {

        if ((cookie == null) || cookie.getValue().isEmpty()) {
            return Response.serverError().entity("ikke innlogget").build();
        }

        try {

            String tocheck;
            tocheck = Jwts.parser().setSigningKey(key).parseClaimsJws(cookie.getValue()).getBody().getSubject();
            if (tocheck.isEmpty()) {
                NewCookie temp = new NewCookie("access_token", null);
                return Response.serverError().cookie(temp).entity("tomt").build();
            }

            //OK, we can trust this JWT
        } catch (SignatureException e) {

            //don't trust the JWT!
            NewCookie temp = new NewCookie("access_token", null);
            return Response.serverError().cookie(temp).entity("noen har tukklet med token signaturen").build();
        }

        return Response.ok("OK").build(); //hva som blir sendt tilbake på suksess.
    }

    /**
     *
     * @param cookie
     * @return
     */
    @Path("/logout")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public Response logout(@CookieParam("access_token") Cookie cookie) {

        NewCookie temp = new NewCookie("access_token", null);

        return Response.ok("OK").cookie(temp).build();
    }

    /**
     *
     * @param cookie
     * @return
     */
    @Path("/isadmin")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response isAdminWeb(@CookieParam("access_token") Cookie cookie) {

        if ((cookie == null) || cookie.getValue().isEmpty()) {
            return Response.ok("ikke innlogget").build();
        }

        try {
            String tocheck;
            tocheck = Jwts.parser().setSigningKey(key).parseClaimsJws(cookie.getValue()).getBody().getSubject();
            if (tocheck.contains("administrator") == true) {
                return Response.ok("true").build(); //hva som blir sendt tilbake på suksess.
            }

            //OK, we can trust this JWT
        } catch (SignatureException e) {

            //don't trust the JWT!
            return Response.ok("noen har tukklet med token signaturen").build();
        }
        String result;

        result = (Jwts.parser().setSigningKey(key).parseClaimsJws(cookie.getValue()).getBody().getSubject());
        return Response.ok("dine roller er: " + result + ". du må være 'administrator'.").build();

    }

    /**
     *
     * @param cookie
     * @return
     */
    @Path("/isarkivansvarlig")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response isArkivansvarligWeb(@CookieParam("access_token") Cookie cookie) {

        if ((cookie == null) || cookie.getValue().isEmpty()) {
            return Response.ok("ikke innlogget").build();
        }

        try {
            String tocheck;
            tocheck = Jwts.parser().setSigningKey(key).parseClaimsJws(cookie.getValue()).getBody().getSubject();
            if (tocheck.contains("arkivansvarlig") == true) {
                return Response.ok("true").build(); //hva som blir sendt tilbake på suksess.
            }

            //OK, we can trust this JWT
        } catch (SignatureException e) {

            //don't trust the JWT!
            return Response.ok("noen har tukklet med token signaturen").build();
        }
        String result;

        result = (Jwts.parser().setSigningKey(key).parseClaimsJws(cookie.getValue()).getBody().getSubject());
        return Response.ok("dine roller er: " + result + ". du må være 'arkivansvarlig'.").build();

    }

    /**
     *
     * @param cookie
     * @return
     */
    @Path("/isutover")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response isUtoverWeb(@CookieParam("access_token") Cookie cookie) {

        if ((cookie == null) || cookie.getValue().isEmpty()) {
            return Response.ok("ikke innlogget").build();
        }

        try {
            String tocheck;
            tocheck = Jwts.parser().setSigningKey(key).parseClaimsJws(cookie.getValue()).getBody().getSubject();
            if (tocheck.contains("utøver") == true) {
                return Response.ok("true").build();
            }

            //OK, we can trust this JWT
        } catch (SignatureException e) {

            //don't trust the JWT!
            return Response.ok("noen har tukklet med token signaturen").build();
        }
        String result;

        result = (Jwts.parser().setSigningKey(key).parseClaimsJws(cookie.getValue()).getBody().getSubject());
        return Response.ok("dine roller er: " + result + ". du må være 'utøver'.").build();

    }

    /**
     *
     * @param cookie
     * @return
     */
    @Path("/isdirigent")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response isDirigentWeb(@CookieParam("access_token") Cookie cookie) {

        if ((cookie == null) || cookie.getValue().isEmpty()) {
            return Response.ok("ikke innlogget").build();
        }

        try {
            String tocheck;
            tocheck = Jwts.parser().setSigningKey(key).parseClaimsJws(cookie.getValue()).getBody().getSubject();
            if (tocheck.contains("dirigent") == true) {
                return Response.ok("true").build(); //hva som blir sendt tilbake på suksess.
            }

            //OK, we can trust this JWT
        } catch (SignatureException e) {

            //don't trust the JWT!
            return Response.ok("noen har tukklet med token signaturen").build();
        }
        String result;

        result = (Jwts.parser().setSigningKey(key).parseClaimsJws(cookie.getValue()).getBody().getSubject());
        return Response.ok("dine roller er: " + result + ". du må være 'dirigent'.").build();

    }

    /**
     *
     * @param cookie
     * @return
     */
    public boolean isAdmin(Cookie cookie) {
        if ((cookie == null) || cookie.getValue().isEmpty()) {
            return false;
        }

        try {
            String tocheck;
            tocheck = Jwts.parser().setSigningKey(key).parseClaimsJws(cookie.getValue()).getBody().getSubject();
            if (tocheck.contains("administrator") == true) {
                return true;
            }

            //OK, we can trust this JWT
        } catch (SignatureException e) {

            //don't trust the JWT!
            return false;
        }
        return false;
    }

    /**
     *
     * @param cookie
     * @return
     */
    public boolean isArkivansvarlig(Cookie cookie) {
        if ((cookie == null) || cookie.getValue().isEmpty()) {
            return false;
        }

        try {
            String tocheck;
            tocheck = Jwts.parser().setSigningKey(key).parseClaimsJws(cookie.getValue()).getBody().getSubject();
            if (tocheck.contains("arkivansvarlig") == true) {
                return true;
            }

            //OK, we can trust this JWT
        } catch (SignatureException e) {

            //don't trust the JWT!
            return false;
        }
        return false;
    }

    /**
     *
     * @param cookie
     * @return
     */
    public boolean isUtover(Cookie cookie) {
        if ((cookie == null) || cookie.getValue().isEmpty()) {
            return false;
        }

        try {
            String tocheck;
            tocheck = Jwts.parser().setSigningKey(key).parseClaimsJws(cookie.getValue()).getBody().getSubject();
            if (tocheck.contains("utøver") == true) {
                return true;
            }

            //OK, we can trust this JWT
        } catch (SignatureException e) {

            //don't trust the JWT!
            return false;
        }
        return false;
    }

    /**
     *
     * @param cookie
     * @return
     */
    public boolean isDirigent(Cookie cookie) {
        if ((cookie == null) || cookie.getValue().isEmpty()) {
            return false;
        }

        try {
            String tocheck;
            tocheck = Jwts.parser().setSigningKey(key).parseClaimsJws(cookie.getValue()).getBody().getSubject();
            if (tocheck.contains("dirigent") == true) {
                return true;
            }

            //OK, we can trust this JWT
        } catch (SignatureException e) {

            //don't trust the JWT!
            return false;
        }
        return false;
    }
}
