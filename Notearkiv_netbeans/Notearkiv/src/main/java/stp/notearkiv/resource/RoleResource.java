package stp.notearkiv.resource;

import stp.notearkiv.entity.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import java.security.Key;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import stp.notearkiv.entity.Role;

/**
 * Class containing methods and queries regarding Role resources
 *
 * @author Team STP
 */
@Stateless
@Path("/roles")
public class RoleResource {

    @PersistenceContext(unitName = "Notearkivet")
    private EntityManager em;
    private final String selectRoles = "SELECT r from Role r";
    private final Key key = Resource.KEY;

    /**
     * Return a list of all registered roles
     *
     * @return a list of all roles
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Role> getAllRoles() {
        return em.createQuery(selectRoles, Role.class).getResultList();
    }

    /**
     * Return a role based on name
     *
     * @param role_name the role name of the wanted role
     * @return a role as json
     */
    @Path("/get/{NAME}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Role getUserByUserName(@PathParam("NAME") String role_name) {
        List result = em.createQuery("SELECT r FROM Role r WHERE (r.name) = (:paramID)", Role.class).setParameter("paramID", role_name).getResultList();
        Role temp = null;
        if (!result.isEmpty()) {
            temp = (Role) result.get(0);
        }

        return temp;
    }

    /**
     *
     * @param id id of the user to change
     * @param role_id list of role_ids to be given to the user
     * @param cookie
     * @return
     */
    @Path("/change")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response change(
            @QueryParam("user_id") int id,
            @QueryParam("role") List<Integer> role_id,
            @CookieParam("access_token") Cookie cookie) {
        if ((cookie == null) || cookie.getValue().isEmpty()) {
            return Response.serverError().entity("ikke innlogget").build();
        }
        try {
            String tocheck;
            tocheck = Jwts.parser().setSigningKey(key).parseClaimsJws(cookie.getValue()).getBody().getSubject();
            //OK, we can trust this JWT
            if ((tocheck.contains("administrator")) || (tocheck.contains("arkivansvarlig")) == true) {
                User temp = em.find(User.class, id);
                if (temp == null) {
                    return Response.serverError().entity("ingen bruker").build();
                }
                if ((role_id == null) || (role_id.isEmpty())) {
                    return Response.serverError().entity("ingen roller er valgt, eller manger 'role='").build();
                }
                String rolesToGet = "";
                if (role_id.size() > 0) {
                    for (int i = 0; i < role_id.size(); i++) {
                        rolesToGet += role_id.get(i);
                        if (i < role_id.size() - 1) {
                            rolesToGet += ",";
                        }
                    }
                }
                List<Role> newRoles;
                newRoles = em.createQuery(selectRoles + " WHERE r.role_id in (" + rolesToGet + ")", Role.class).getResultList();
                if (newRoles != null) {
                    if (!newRoles.isEmpty()) {
                        temp.setRoles(newRoles);
                        temp.updateDate_edited();
                        em.persist(temp);
                        String ret = temp.getFirst_name() + " " + temp.getLast_name() + " har fått ny rolle: ";
                        for (Role r : newRoles) {
                            ret += r.getName() + " ";
                        }
                        return Response.ok(ret).build(); //hva som blir sendt tilbake på suksess.
                    }
                }
                return Response.serverError().entity("Ingen gyldige roller er valgt").build(); //hva som blir sendt tilbake på suksess.
            }

        } catch (SignatureException e) {
            //don't trust the JWT!
            return Response.serverError().entity("noen har tukklet med token signaturen").build();
        }
        return Response.serverError().entity("du har ikke rettigheter til å gjøre dette.").build();
    }
}
