package stp.notearkiv.resource;

import stp.notearkiv.entity.Score;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import javax.inject.Inject;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Cookie;
import stp.notearkiv.entity.Musician;
import stp.notearkiv.entity.Part;

/**
 * Resource class for handling Score objects.
 *
 * @author Team STP
 */
@Stateless
@Path("/scores")
public class ScoreResource {

    @Inject
    ImportResource ir;

    @Inject
    SessionResource sr;

    @PersistenceContext(unitName = "Notearkivet")
    private EntityManager em;
    private final String selectScores = "select s from Score s";
    private final Key key = Resource.KEY;

    /**
     *
     * @return list of all the scores in DB
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Score> getAllScores() {
        return em.createQuery(selectScores, Score.class).getResultList();
    }

    /**
     *
     * @param search string to search for
     * @param columns columns to search in
     * @return
     */
    @Path("/find")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Score> search(
            @DefaultValue("") @QueryParam("search") String search,
            @DefaultValue("title") @QueryParam("columns") List<String> columns) {

        String ScoresToGet = "";
        if (columns.size() > 0) {
            for (int i = 0; i < columns.size(); i++) {

                if ((columns.get(i).equalsIgnoreCase("archive_number")) || columns.get(i).equalsIgnoreCase("score_id")) {
                    ScoresToGet += "(s." + columns.get(i) + ") = " + search;
                    if (i < columns.size() - 1) {
                        ScoresToGet += " or ";
                    }
                } else {

                    ScoresToGet += "lower (s." + columns.get(i) + ") like lower ('%" + search + "%')";
                    if (i < columns.size() - 1) {
                        ScoresToGet += " or ";
                    }
                }
            }
        }
        List<Score> foundScores;
        foundScores = em.createQuery(selectScores + " WHERE (" + ScoresToGet + ")", Score.class).getResultList();

        if (foundScores != null) {
            if (!foundScores.isEmpty()) {
                return foundScores;
            }
        }
        List<Score> error;
        error = new ArrayList<>();
        return error;
    }

    /**
     *
     * @param id the scoreid of the wanted Score
     * @return a Score as json
     */
    @Path("/{SCORE_ID}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Score getScore(@PathParam("SCORE_ID") int id) {
        return em.find(Score.class, id);
    }

    /**
     *
     * @param cookie
     * @param archive_number
     * @param title
     * @param label
     * @param duration
     * @param grade
     * @param comment
     * @param pdfpath
     * @param mp3path
     * @param score_composers
     * @param score_arrangers
     * @param lyricists
     * @param score_artists
     * @param parts
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    @Path("/add")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String addScore(@CookieParam("access_token") Cookie cookie,
            @FormParam("archive_number") int archive_number,
            @FormParam("title") String title,
            @FormParam("label") String label,
            @FormParam("duration") String duration,
            @FormParam("grade") String grade,
            @FormParam("comment") String comment,
            @FormParam("pdfpath") String pdfpath,
            @FormParam("mp3path") String mp3path,
            @FormParam("score_composers") String score_composers,
            @FormParam("score_arrangers") String score_arrangers,
            @FormParam("score_lyricists") String lyricists,
            @FormParam("score_artists") String score_artists,
            @FormParam("parts") String parts) throws NoSuchAlgorithmException, InvalidKeySpecException {

        if (duration.isEmpty()) {
            duration = "00:00:00";
        }

        List<Part> newParts = new ArrayList<>();
        if (!parts.isEmpty()) {
            newParts = em.createQuery("select p from Part p WHERE p.part_id in (" + parts + ")", Part.class).getResultList();
        }

        List<Score> scoreToSend = new ArrayList<>();

        List<Musician> score_composers_send = new ArrayList<>();
        Musician temp = new Musician(score_composers);
        score_composers_send.add(temp);

        List<Musician> score_arrangers_send = new ArrayList<>();
        Musician temp1 = new Musician(score_arrangers);
        score_arrangers_send.add(temp1);

        List<Musician> score_lyricists_send = new ArrayList<>();
        Musician temp2 = new Musician(lyricists);
        score_lyricists_send.add(temp2);

        List<Musician> score_artists_send = new ArrayList<>();
        Musician temp3 = new Musician(score_artists);
        score_artists_send.add(temp3);

        Score tempScore = new Score(archive_number, title, label, Time.valueOf(duration), grade, comment, pdfpath, mp3path, score_composers_send, score_arrangers_send, score_lyricists_send, score_artists_send, newParts);

        scoreToSend.add(tempScore);
        Score[] input = new Score[scoreToSend.size()];
        int i = 0;
        for (Score s : scoreToSend) {
            input[i] = s;
            i++;
        }

        String ret = ir.importScores(cookie, input);
        return ret;
    }

    /**
     *
     * @param id
     * @param cookie
     * @param archive_number
     * @param title
     * @param label
     * @param duration
     * @param grade
     * @param comment
     * @param pdfpath
     * @param mp3path
     * @param score_composers
     * @param score_arrangers
     * @param lyricists
     * @param score_artists
     * @param parts
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    @Path("/alterscore/{SCORE_ID}")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Score> alterScore(@PathParam("SCORE_ID") int id,
            @CookieParam("access_token") Cookie cookie,
            @FormParam("archive_number") int archive_number,
            @FormParam("title") String title,
            @FormParam("label") String label,
            @FormParam("duration") String duration,
            @FormParam("grade") String grade,
            @FormParam("comment") String comment,
            @FormParam("pdfpath") String pdfpath,
            @FormParam("mp3path") String mp3path,
            @FormParam("score_composers") String score_composers,
            @FormParam("score_arrangers") String score_arrangers,
            @FormParam("score_lyricists") String lyricists,
            @FormParam("score_artists") String score_artists,
            @FormParam("parts") String parts) throws NoSuchAlgorithmException, InvalidKeySpecException {

        if (!sr.isAdmin(cookie) && !sr.isArkivansvarlig(cookie)) {
            List<Score> no = new ArrayList<>();
            return no;
        }
        Score testme = getScore(id);
        Score me = em.find(Score.class, testme.getScore_id());
        me.setArchive_number(archive_number);
        me.setTitle(title);
        me.setLabel(label);
        me.setDuration(Time.valueOf(duration));
        me.setGrade(grade);
        me.setComment(comment);
        me.setPdfpath(pdfpath);
        me.setMp3path(mp3path);

        try {
            List<Musician> scorecomposerslinked = new ArrayList<>();

            List<String> list = new ArrayList<>(Arrays.asList(score_composers.split("(\\s*,\\s*)|(\\s*/\\s*)|(\\s* - \\s*)|(\\s*&amp;\\s*)|(\\s*&\\s*)|(\\s* and \\s*)|(\\s* og \\s*)")));
            for (String names : list) {
                List temp = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                if (!temp.isEmpty()) {
                    scorecomposerslinked.add((Musician) temp.get(0));
                } else {
                    em.persist(new Musician(names));
                    List temp1 = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                    if (!temp1.isEmpty()) {
                        scorecomposerslinked.add((Musician) temp1.get(0));
                    }
                }
            }

            me.setScore_composers(scorecomposerslinked);
        } catch (NullPointerException e) {

            System.out.println("no composer");
        }

        try {
            List<Musician> scorearrangerslinked = new ArrayList<>();

            List<String> list = new ArrayList<>(Arrays.asList(score_arrangers.split("(\\s*,\\s*)|(\\s*/\\s*)|(\\s* - \\s*)|(\\s*&amp;\\s*)|(\\s*&\\s*)|(\\s* and \\s*)|(\\s* og \\s*)")));
            for (String names : list) {
                List temp = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                if (!temp.isEmpty()) {
                    scorearrangerslinked.add((Musician) temp.get(0));
                } else {
                    em.persist(new Musician(names));
                    List temp1 = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                    if (!temp1.isEmpty()) {
                        scorearrangerslinked.add((Musician) temp1.get(0));
                    }
                }
            }

            me.setScore_arrangers(scorearrangerslinked);
        } catch (NullPointerException e) {

            System.out.println("no arrangers");
        }

        try {
            List<Musician> scorelyricistslinked = new ArrayList<>();

            List<String> list = new ArrayList<>(Arrays.asList(lyricists.split("(\\s*,\\s*)|(\\s*/\\s*)|(\\s* - \\s*)|(\\s*&amp;\\s*)|(\\s*&\\s*)|(\\s* and \\s*)|(\\s* og \\s*)")));
            for (String names : list) {
                List temp = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                if (!temp.isEmpty()) {
                    scorelyricistslinked.add((Musician) temp.get(0));
                } else {
                    em.persist(new Musician(names));
                    List temp1 = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                    if (!temp1.isEmpty()) {
                        scorelyricistslinked.add((Musician) temp1.get(0));
                    }
                }
            }

            me.setScore_lyricists(scorelyricistslinked);
        } catch (NullPointerException e) {

            System.out.println("no lyracist");
        }
        try {
            List<Musician> scoreartistslinked = new ArrayList<>();

            List<String> list = new ArrayList<>(Arrays.asList(score_artists.split("(\\s*,\\s*)|(\\s*/\\s*)|(\\s* - \\s*)|(\\s*&amp;\\s*)|(\\s*&\\s*)|(\\s* and \\s*)|(\\s* og \\s*)")));
            for (String names : list) {
                List temp = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                if (!temp.isEmpty()) {
                    scoreartistslinked.add((Musician) temp.get(0));
                } else {
                    em.persist(new Musician(names));
                    List temp1 = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                    if (!temp1.isEmpty()) {
                        scoreartistslinked.add((Musician) temp1.get(0));
                    }
                }
            }

            me.setScore_artists(scoreartistslinked);
        } catch (NullPointerException e) {

            System.out.println("no artists");
        }
        try {
            // add parts connected to the score
            List<Part> newParts = new ArrayList<>();
            if (!parts.isEmpty()) {
                newParts = em.createQuery("select p from Part p WHERE p.part_id in (" + parts + ")", Part.class).getResultList();
                if (!newParts.isEmpty()) {
                    me.setParts(newParts);
                }
            } else {
                me.setParts(null);
            }
        } catch (NullPointerException e) {
            System.out.println("no score");
        } catch (IllegalArgumentException e) {

        }
        em.persist(me);
        return em.createQuery(selectScores, Score.class).getResultList();
    }
}
