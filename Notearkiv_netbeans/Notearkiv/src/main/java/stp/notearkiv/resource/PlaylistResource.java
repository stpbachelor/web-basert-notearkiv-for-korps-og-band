package stp.notearkiv.resource;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Resource class for handling Playlist objects.
 *
 * @author Team STP
 */
@Stateless
//@Path("/playlists")
public class PlaylistResource {

    @PersistenceContext(unitName = "Notearkivet")
    private EntityManager em;
}
