package stp.notearkiv.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Class containing objects of type Role.
 *
 * @author Team STP
 */

//@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "DBRole")

public class Role implements Serializable{
    @Id @GeneratedValue
    @NotNull private int role_id;
    @NotNull private String name;
    
    @ManyToMany(fetch=FetchType.LAZY, mappedBy = "roles")
    private List<User> users;

    /**
     * Creates an empty Role object.
     */
    public Role() {
    }

    /**
     * creates a new Role object.
     * 
     * @param name The name or description of the role
     */
    public Role(String name) {
        this.name = name;
    }
     public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }
    
}
