package stp.notearkiv.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

/**
 * Data class holding information about Musician objects.
 *
 * @author Team STP
 */

@Entity
public class Musician implements Serializable {
    //Generating a primary key for this table. This id can not be null. 
    @Id @GeneratedValue
    @NotNull private int musician_id;
   
    @NotNull private String name;
    
    @ManyToMany(fetch=FetchType.LAZY, mappedBy = "score_composers")
    private transient List<Score> score_comp;
    
    @ManyToMany(fetch=FetchType.LAZY, mappedBy = "score_artists")
    private transient List<Score> score_art;
    
    @ManyToMany(fetch=FetchType.LAZY, mappedBy = "score_arrangers")
    private transient List<Score> score_arr;
    
    @ManyToMany(fetch=FetchType.LAZY, mappedBy = "score_lyricists")
    private transient List<Score> score_lyr;
    
    /**
     *creates an empty Musician object.
     */
    public Musician(){   
    }

    /*
     * @param muscian_id
     * @param first_name
     * @param last_name
     */
    public Musician(String name){
         
         this.name = name;
    }
    // The following is standard setter and getter methods
    
    public int getMusician_id() {
        return musician_id;
    }

    public void setMusician_id(int musician_id) {
        this.musician_id = musician_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Score> getScore_comp() {
        return score_comp;
    }

    public List<Score> getScore_art() {
        return score_art;
    }

    public List<Score> getScore_arr() {
        return score_arr;
    }

    public List<Score> getScore_lyr() {
        return score_lyr;
    } 
}
