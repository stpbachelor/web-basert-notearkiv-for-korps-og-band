package stp.notearkiv.entity;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import stp.notearkiv.PasswordEncryptionService;

/**
 * Data class holding information about user objects.
 *
 * @author Team STP
 */
//@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "DBUser")
public class User implements Serializable {

    @Id @GeneratedValue
    @NotNull private int user_id;
    @NotNull private String first_name, last_name, street_address, city, email, user_name;  
    @NotNull private int postal_code, phone, login_count;
    @NotNull private byte[] password, salt;
    private String date_member;
    boolean active;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date_edited, date_login;

    @Temporal(javax.persistence.TemporalType.DATE)
    private final Date date_created = new Date();
    
    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(
        name = "USER_ROLE",
        joinColumns = @JoinColumn(name = "USER_ROLE_ID", referencedColumnName = "USER_ID"),
        inverseJoinColumns = @JoinColumn(name = "ROLE_USER_ID", referencedColumnName = "ROLE_ID"))
    private List<Role> roles;
   
    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(
        name = "USER_INSTRUMENT",
        joinColumns = @JoinColumn(name = "USER_INSTRUMENT_ID", referencedColumnName = "USER_ID"),
        inverseJoinColumns = @JoinColumn(name = "INSTRUMENT_USER_ID", referencedColumnName = "INSTRUMENT_ID"))
    private List<Instrument> instruments;
    
    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(
        name = "USER_PART",
        joinColumns = @JoinColumn(name = "USER_PART_ID", referencedColumnName = "USER_ID"),
        inverseJoinColumns = @JoinColumn(name = "PART_USER_ID", referencedColumnName = "PART_ID"))
    private List<Part> myparts;
    
    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(
        name = "USER_PLAYLIST",
        joinColumns = @JoinColumn(name = "USER_PLAYLIST_ID", referencedColumnName = "USER_ID"),
        inverseJoinColumns = @JoinColumn(name = "PLAYLIST_USER_ID", referencedColumnName = "PLAYLIST_ID"))
    private List<Playlist> playlists;

    /**
     * Creates an empty User object.
     */
    public User() {
    }
    
    /**
     * Creates an User object including all variables.
     *
     * @param first_name
     * @param last_name
     * @param street_address
     * @param postal_code
     * @param city
     * @param email
     * @param phone
     * @param date_member
     * @param userInstruments
     * @param user_name
     * @param password1
     * @param userRoles
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.security.spec.InvalidKeySpecException
     */
    public User(
            String first_name, 
            String last_name, 
            String street_address, 
            int postal_code, 
            String city, 
            String email, 
            int phone, 
            String date_member,
            List<Instrument> userInstruments,
            String user_name, 
            String password1, 
            List<Role> userRoles
    ) throws NoSuchAlgorithmException, InvalidKeySpecException {
        this.first_name = first_name;
        this.last_name = last_name;
        this.street_address = street_address;
        this.postal_code = postal_code;
        this.city = city;
        this.email = email;
        this.phone = phone;
        this.date_member = date_member;
        this.instruments = userInstruments;
        this.user_name = user_name;
        this.roles = userRoles;
        this.login_count = 0;
        this.active =true;

        PasswordEncryptionService a = new PasswordEncryptionService();

        this.salt = a.generateSalt();
        this.password = a.getEncryptedPassword(password1, salt);
    }

    public int getUser_id() {
        return user_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getStreet_address() {
        return street_address;
    }

    public int getPostal_code() {
        return postal_code;
    }

    public String getCity() {
        return city;
    }

    public int getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getUser_name() {
        return user_name;
    }

    public byte[] getPassword() {
        return password;
    }

    public byte[] getSalt() {
        return salt;
    }

    public String getDate_member() {
        return date_member;
    }

    public Date getDate_created() {
        return date_created;
    }

    public Date getDate_edited() {
        return date_edited;
    }

    public Date getDate_login() {
        return date_login;
    }

    public int getLogin_count() {
        return login_count;
    }
    
    public void updateLogin_count() {
        this.login_count += 1;
    }

    public List<Instrument> getInstruments() {
        return instruments;
    }

    public void setInstruments(List<Instrument> instruments) {
        this.instruments = instruments;
    }
    

    public List<Playlist> getPlaylists() {
        return playlists;
    }
    
    public List<Role> getRoles() {
        return roles;
    }
    public void setRoles(List<Role>newRoles){
        this.roles = newRoles;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setStreet_address(String street_address) {
        this.street_address = street_address;
    }

    public void setPostal_code(int postal_code) {
        this.postal_code = postal_code;
    }

    public void setCity(String city) {
        this.city = city.toUpperCase();
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public void setDate_member(String date_member) {
        this.date_member = date_member;
    }

    public void updateDate_edited() {
        this.date_edited = new Date();
    }

    public void updateDate_login() {
        this.date_login = new Date();
    }

    public boolean isActive() {
        return active;
    }

    public void setIsActive(boolean active) {
        this.active = active;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setLogin_count(int login_count) {
        this.login_count = login_count;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setDate_edited(Date date_edited) {
        this.date_edited = date_edited;
    }

    public void setDate_login(Date date_login) {
        this.date_login = date_login;
    }

    public void setPlaylists(List<Playlist> playlists) {
        this.playlists = playlists;
    }

    public List<Part> getMyparts() {
        return myparts;
    }

    public void setMyparts(List<Part> myparts) {
        this.myparts = myparts;
    }
    
}