package stp.notearkiv.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Team STP
 */
@Entity
public class Instrument implements Serializable {
    @Id @GeneratedValue
    @NotNull private int instrument_id;
    @NotNull private String name;
    
    @ManyToMany(fetch=FetchType.LAZY, mappedBy = "instruments")
    private List<User> users;

    public Instrument() {
    }

    public Instrument(String name) {
        this.name = name;
    }

    public void setInstrument_id(int instrument_id) {
        this.instrument_id = instrument_id;
    }
    

    public int getInstrument_id() {
        return instrument_id;
    }

    public String getName() {
        return name;
    }
    
    public List<User> getUsers() {
        return users;
    }

    public void setName(String name) {
        this.name = name;
    }
}