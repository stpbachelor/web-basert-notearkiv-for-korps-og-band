/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function logout(e) {
    var url = "services/logout";

    $.ajax({
        type: "POST",
        url: url,
        success: function (data)
        {
            if (data.valueOf() === "OK") {
                window.location = "login.html";
            }
        }
    });
    e.preventDefault();
}

function basicSessionCheck() {
    var service2 = 'services/session';
    jQuery.support.cors = true;
    $.ajax(
            {
                type: "GET",
                url: service2,
                data: "{}",
                contentType: "application/json",
                dataType: 'text',
                cache: false,
                success: function (data) {
                    if (data.valueOf() === "OK") {
                        $('body').show();

                    }
                },
                error: function () {
                    window.location = "login.html";
                }
            });
}

function sessionCheck() {
    var service2 = 'services/session';
    jQuery.support.cors = true;
    $.ajax(
            {
                type: "GET",
                url: service2,
                data: "{}",
                contentType: "application/json",
                dataType: 'text',
                cache: false,
                success: function (data) {
                    if (data.valueOf() === "OK") {

                        authorize();
                    }
                },
                error: function () {
                    window.location = "login.html";
                }
            });
}

function authorize() {
    $.when(ajax1(), ajax2()).done(function (a1, a2) {
        if ((a1[0].valueOf() === "true") || (a2[0].valueOf()) === "true") {
            $('body').show();
        } else {
            window.location = "feil.html";
        }
    });
    function ajax1() {
        var service1 = 'services/isadmin';
        jQuery.support.cors = true;
        return $.ajax(
                {
                    type: "GET",
                    url: service1,
                    data: "{}",
                    contentType: "application/json",
                    dataType: 'text',
                    cache: false
                });
    }
    function ajax2() {
        var service2 = 'services/isarkivansvarlig';
        jQuery.support.cors = true;
        return $.ajax(
                {
                    type: "GET",
                    url: service2,
                    data: "{}",
                    contentType: "application/json",
                    dataType: 'text',
                    cache: false
                });
    }
}